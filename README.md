#### Assistants meeting
Wednesday, 2:00 PM, break room.
At the start of each month we will all meet with Jean at 12pm
for lunch.

#### Progress in lecture classes
* Wed 17 Sep. Up to the end of Chapter 1.
* Wed 24 Sep. Up to the end of Chapter 2. Skipped countability.
* Wed 1 Oct. Up to end of chapter on functions. Included countability
  and Cantor argument for uncountability of **R**.
* Wed 8 Oct. Up to the definition of Cauchy sequence and statement of the
  theorem that a Cauchy sequence converges.
* Wed 15 Oct. Up to statements (but not proofs) of Satz 6.5.3 and 6.6.3 (note,
  JB is doing this part in a different order and the theorem on Cauchy
  products arises as a lemma to Satz 6.6.3 instead.)
* Wed 22 Oct. On schedule. Did uniform continuity.
* Wed 5 Nov. Up to statement but not proof of Korollar 8.4.I.

#### Assistant list
- Gabriel Berzunza Ojeda gabriel.berzunza@math.uzh.ch
- Cyril Marzouk cyril.marzouk@math.uzh.ch
- Alexander Watson * alexander.watson@math.uzh.ch
- Severin Schraven * severin.schraven@uzh.ch
- Timo Schmidt * (PHY) tschmidt@physik.uzh.ch
- Matthieu Jaquier * (PHY) matthieu@physik.uzh.ch

#### Class assignments
* Mon 08:00. **Gabriel**.
* Mon 15:00. **Severin**.
* Tue 08:00. **Cyril**.
* Thu 08:00. **Matthieu**.
* Thu 10:15. **Timo**.
* Fri 15:00. **Alex**.

I set up trays for Severin, Timo and Matthieu on top of the letterboxes on
floor K. Unreturned student work goes in a tray on floor K.

#### Exercise sheet assignments
* Week 1. **Cyril**. Circulate Fri 12 Sep. Upload Mon 15 Sep.
  Deposit Fri 19 Sep (noon). The first sheet is a practice sheet and should not
  be entered in the system.
* Week 2. **Alex**. Circulate Tue 16 Sep. Upload Thu 18 Sep. Deposit Thu 25 Sep (5pm).
* Week 3. **Matthieu**. Circulate Tue 23 Sep. Upload Thu 25 Sep. Deposit Thu 2 Oct (5pm).
* Week 4. **Timo**. Circulate Tue 30 Sep. Upload Thu 2 Oct. Deposit Thu 9 Oct (5pm).
* Week 5. **Gabriel**. Circulate Tue 7 Oct. Upload Thu 9 Oct. Deposit Thu 16 Oct (5pm).
* Week 6. **Severin**. Circulate Tue 14 Oct. Upload Thu 16 Oct. Deposit Thu 23 Oct (5pm).
* Week 7. **Quan**. Circulate Tue 21 Oct. Upload Thu 23 Oct. Deposit Thu 30 Oct (5pm).
* Week 8. **Cyril**. Circulate Tue 28 Oct. Upload Thu 30 Oct. Deposit Thu 6 Nov (5pm).
* Week 9. **Alex**. Circulate Tue 4 Nov. Upload Thu 6 Nov. Deposit Thu 13 Nov (5pm).
* Week 10. **Matthieu**. Circulate Tue 11 Nov. Upload Thu 13 Nov. Deposit Thu 20 Nov (5pm).
* Week 11. **Timo**. Circulate Tue 18 Nov. Upload Thu 20 Nov. Deposit Thu 27 Nov (5pm).
* Week 12. **Gabriel**. Circulate Tue 25 Nov. Upload Thu 27 Nov. Deposit Thu 4 Dec (5pm).
* Week 13. **Severin**. Circulate Tue 2 Dec. Upload Thu 4 Dec. Deposit Thu 11 Dec (5pm).
* Week 14. **Quan**. Circulate Tue 9 Dec. Upload Thu 11 Dec. Deposit Thu 18 Dec (5pm).
  This sheet will be marked and solutions posted on the web, but there will be no class
  associated with it.

#### Marking for Quan
* Sheet 3 (students deposit 2 Oct): Gabriel
* Sheet 4 (students deposit 9 Oct): Severin
* Sheet 5 (students deposit 16 Oct): nobody (you are writing sheet 7 this week)
* Sheet 6 (students deposit 23 Oct): nobody
* Sheet 7 (students deposit 30 Oct): Cyril
* Sheet 8 (students deposit 6 Nov): Alex and Matthieu
* Sheet 9 (students deposit 13 Nov): Timo
* Sheet 10 (students deposit 20 Nov): Gabriel
* Sheet 11 (students deposit 27 Nov): Severin
* Sheet 12 (students deposit 4 Dec): nobody (you are writing sheet 14 this week)
* Sheet 13 (students deposit 11 Dec): Cyril
* Sheet 14 (students deposit 18 Dec): Alex